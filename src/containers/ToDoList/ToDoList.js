import React, {Component} from 'react';
import axios from 'axios';
import './ToDoList.css';

class ToDoList extends Component {
    state = {
        tasks: [],
        text: ''
    };

    componentDidMount() {
        axios.get('/tasks.json').then(response => {
            console.log(response);
            const tasks = [];
            for (let key in response.data) {
                tasks.push({...response.data[key], id: key});
            }
            this.setState({tasks});
        });
    }

    handleChange = (event) => {
        this.setState({text: event.target.value});
    };

    handleClick = (e) => {
        e.preventDefault();
        if (this.state.text !== '') {
            axios.post('/tasks.json', {task: this.state.text}).then((response) => {
                const tasks = [...this.state.tasks];
                tasks.push({task: this.state.text, id: response.data.name});
                this.setState({tasks});
            });
        } else {
            alert('Please add a task!');
        }
    };

    removeTask = (id) => {
        const index = this.state.tasks.findIndex(div => div.id === id);
        const tasks = [...this.state.tasks];
        axios.delete(`/tasks/${tasks[index].id}.json`);
        tasks.splice(index, 1);
        this.setState({tasks});
    };

    render() {
        return (
            <div className="ToDoList">
                <form>
                    <input placeholder="Your task ..." value={this.state.text} onChange={this.handleChange} id="task"
                           type="text"/>
                    <button onClick={this.handleClick} id="add"><i className="fa fa-arrow-circle-down"
                                                                   aria-hidden="true"/></button>
                </form>
                {this.state.tasks.map((task, k) => <div className="txt" key={k}>{task.task}<i
                    onClick={() => this.removeTask(task.id)} className="fa fa-trash-o" aria-hidden="true"/></div>)}
            </div>
        );
    }
}

export default ToDoList;