import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import './HomePage.css';

class HomePage extends Component {
    render() {
        return(
            <div className="choose-container">
                <NavLink to="/ToDoList" className="box">ToDoList</NavLink>
                <NavLink to="/MoviesList" className="box">MoviesList</NavLink>
            </div>
        )
    }
}

export default HomePage;