import React, {Component} from 'react';
import axios from 'axios';
import './MoviesList.css';
import Films from './components/Films/Films';

class MoviesList extends Component {
    state = {
        films: [],
        film: ''
    };

    componentDidMount() {
        axios.get('/films.json').then(response => {
            const films = [];
            for (let key in response.data) {
                films.push({...response.data[key], id: key});
            }
            this.setState({films});
            console.log(this.state.films);
        });
    }

    handleChange = (event) => {
        this.setState({film: event.target.value});
    };

    handleClick = (e) => {
        e.preventDefault();
        if (this.state.film !== '') {
            axios.post('/films.json', {film: this.state.film}).then((response) => {
                const films = [...this.state.films];
                films.push({film: this.state.film, id: response.data.name});
                this.setState({films});
            });
        } else {
            alert('Please add a film!');
        }
    };

    changePost = (event, index) => {
        let filmIndex;
        const films = [...this.state.films];
        films.forEach((item, i) => item.id === index ? filmIndex = i : null);

        const film = {...this.state.films[filmIndex]};
        console.log(film);
        console.log(event.target);
        film.film = event.target.value;

        films[filmIndex] = film;
        this.setState({films});
    };

    removeFilm = (id) => {
        /*console.log(this.state.films);
        console.log(id);
        const index = this.state.films.findIndex(film => film.id === id);*/
        const films = [...this.state.films];
        console.log(id);
        axios.delete(`/films/${films[id].id}.json`);
        films.splice(id, 1);
        this.setState({films});
    };

    render() {
        return (
            <div className="App">
                <form>
                    <input placeholder="Film ..." value={this.state.film} onChange={this.handleChange} id="task"
                           type="text"/>
                    <button onClick={this.handleClick} id="add">Add</button>
                </form>
                {this.state.films.map((film, k) =>
                    <Films key={k} change={(event) => this.changePost(event, film.id)}
                           removeFilm={() => this.removeFilm(k)} text={film.film}/>
                )}
            </div>
        );
    }
}

export default MoviesList;