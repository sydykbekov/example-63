import React, {Component} from 'react';

class Films extends Component {
    shouldComponentUpdate(nextProps) {
        return nextProps.film !== this.props.film;
    }

    render() {
        return (
            <div id="container">
                <div className="txt">
                    <input className="film" onChange={this.props.change} value={this.props.text} type="text"/>
                    <i className="fa fa-trash-o" aria-hidden="true" onClick={this.props.removeFilm} />
                </div>
            </div>
        )
    }
}

export default Films;